import meraki
import os
import yaml

api_key = os.getenv("MERAKI_KEY")
org_id = os.getenv("ORG_ID")

def get_nets():
    networks = dash.organizations.getOrganizationNetworks(org_id)
    net_map = {net['id']: net['name'] for net in networks}
    devices = dash.organizations.getOrganizationDevices(org_id)
    return net_map, devices


def make_inv(net_map, devices):
    inventory = {"all":{"children":{"networks":{"hosts":{}},"sites":{"children":{}}}}}
    for dev in devices:
        net_name = net_map[dev['networkId']]
        ptype = dev['productType']
        dev_name = dev['name']
        if dev_name == "": dev_name = dev['mac']
        if net_name in inventory['all']['children']['sites']['children']:
            inventory['all']['children']['sites']['children'][net_name]['hosts'].update({dev_name:""})
        else: 
            inventory['all']['children']['sites']['children'].update({net_name:{"hosts":{f"{net_name}-net":""}}})
            inventory['all']['children']['sites']['children'][net_name]['hosts'].update({dev_name:""})
        if ptype in inventory['all']['children']:
            inventory['all']['children'][ptype]['hosts'].update({dev_name:""})
        else:
            inventory['all']['children'].update({ptype:{"hosts":{dev_name:""}}})
    for net in net_map:
        inventory['all']['children']['networks']['hosts'].update({f"{net_map[net]}-net":""})

    return inventory


def create_files(inv, nets, devices):
    invfolder = "meraki-inv"
    os.makedirs(invfolder, exist_ok=True)
    os.makedirs(os.path.join(invfolder, "host_vars"), exist_ok=True)
    os.makedirs(os.path.join(invfolder, "group_vars"), exist_ok=True)
    with open(f"{invfolder}/hosts.yaml", 'w') as file:
        yaml.dump(inv, file)
    for dev in devices:
        dev_name = dev['name']
        if dev_name == "": dev_name = dev['mac']
        with open(f"{invfolder}/host_vars/{dev_name}.yaml", 'w') as file:
            file.write(f"sn: {dev['serial']}\n")
    for id in nets:
        with open(f"{invfolder}/group_vars/{nets[id]}.yaml", 'w') as file:
            file.write(f"networkId: {id}")
    return


if __name__ == "__main__":
    dash = meraki.DashboardAPI(api_key, suppress_logging=True)
    net_map, devices = get_nets()
    inventory = make_inv(net_map, devices)
    create_files(inventory, net_map, devices)
    #print(inventory)